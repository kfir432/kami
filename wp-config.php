<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kami_db');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '40K{DV 7ILtIDT<*.plpD-|HTF|8/hcM*{. |o?WlKGg/CvfMb^e2R%42NYs s;q');
define('SECURE_AUTH_KEY',  '_?l2a-%I]p0pyq(fhkw.ww$q$*r?-_0K=l:%-.E@$?`PO3a7MrKAGf_xdI.Ik^wt');
define('LOGGED_IN_KEY',    'dX:o5JP$NF vD BTB0!8q,D3.Px`O59[AwGo,u?t 53mx+cjk|C2Q.@kID0A/qW5');
define('NONCE_KEY',        ';A>.$372b9e.4!^B^p5<I/~c0]0Rl@fLn^U;B3vH2AGoVLiz+)n)622quS}YXjP7');
define('AUTH_SALT',        'U,GL2gm-<J`| )j^q`-UpoJVPv@:C)gnKu>-AjQQ{yuPM.g`b>eE1JS#.lNlF;=$');
define('SECURE_AUTH_SALT', ';jpI]pd/15nE{vNHFhv3AEb1 GVT0,,Jp/Iu7yfr.<ls!~N9v,!LxN!Q_a+Y;(MT');
define('LOGGED_IN_SALT',   'ZLd=A$LuNxrwWl3Wd4b&kULGZL!{/hd13G_!H[O_%SD%G{1V#CQ>pucI@Bmv,vz%');
define('NONCE_SALT',       'L:Hw4>Hj: qGMd`?0e:T0)bk*]j9M3CCNP^lb^|}LWNVFi >_{?I}&C,n~_8:Rwm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
