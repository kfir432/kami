<!-- Carousel -->
<div class="carousel slide col-md-8 col-sm-7 col-xs-12" data-ride="carousel" id="quote-carousel">
    <div class="carousel-control-wrapper">
        <div class="sub-header inline-block">
            <h3 class="pink-color"><?php _e('Testimonials','twentyseventeen') ?></h3>
        </div>
        <div class="carousel-control-wrap">
            <a class="right carousel-control" href="#quote-carousel" role="button" data-slide="next">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
            <a class="left carousel-control" href="#quote-carousel" role="button" data-slide="prev">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </a>
        </div>
        <hr/>
    </div>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <!-- Controls -->


        <?php
        // Item size (set here the number of posts for each group)
        $i = 2;

        // Set the arguments for the query
        global $post;


        $args = array(
            'numberposts'   => -1, // -1 is for all
            'post_type'     => 'clients', // or 'post', 'page'
            'orderby'       => 'title', // or 'date', 'rand'
            'order' 	      => 'ASC', // or 'DESC'
        );

        // Get the posts
        $myposts = get_posts($args);

        // If there are posts
        if($myposts):

            // Groups the posts in groups of $i
            $chunks = array_chunk($myposts, $i);
            $html = "";

            /*
             * Item
             * For each group (chunk) it generates an item
             */
            foreach($chunks as $chunk):
                // Sets as 'active' the first item
                ($chunk === reset($chunks)) ? $active = "active" : $active = "";
                $html .= '<div class="item '.$active.'"><div><div class="row">';

                /*
                 * Posts inside the current Item
                 * For each item it generates the posts HTML
                 */
                foreach($chunk as $post):
                    $content = get_the_excerpt();
                    $out = strlen($content) > 250 ? mb_substr($content,0,250)."..." : $content;

                    $html .= '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">';
                    $html .= '<div class="carousel-item">';
                    $html .= get_the_post_thumbnail($post->ID);
                    $html .= '<p>'.$out.'</p>';
                    $html .= get_the_title($post->ID);
                    $html .= '</div>';
                    $html .= '</div>';
                endforeach;

                $html .= '</div></div></div>';

            endforeach;

            // Prints the HTML
            echo $html;

        endif;
        ?>

    </div> <!-- carousel inner -->

</div> <!-- /carousel -->
