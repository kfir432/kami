<?php
/**
 * Template for displaying search forms in Twenty Seventeen
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <fieldset>
        <div class="input-group">
            <input type="text" name="s" id="search" placeholder="<?php _e("Search", "twentyseventeen"); ?>" value="<?php the_search_query(); ?>" class="form-control" />
            <span class="input-group-btn">
				<button type="submit" class="btn btn-primary"><?php _e("Search", "twentyseventeen"); ?></button>
			</span>
        </div>
    </fieldset>
</form>