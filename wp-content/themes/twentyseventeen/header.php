<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

    <header>
        <div id="top-strip">
            <div class="col-md-6 col-sm-6 col-xs-12 contact-line-wrapper">
                <div class="contact-line">
                    <a href="mailto:<?php _e('Email Address', 'twentyseventeen'); ?>">
                        <i class="fa fa-envelope"></i>
                        <span><?php _e('Email Address', 'twentyseventeen'); ?></span>
                    </a>
                </div>
                <div class="contact-line">
                    <a href="tel:<?php _e('Phone Number', 'twentyseventeen'); ?>">
                        <i class="fa fa-phone"></i>
                        <span>
                                <?php _e('Contact Us', 'twentyseventeen'); ?>
                                <?php _e('Phone Number', 'twentyseventeen'); ?>
                            </span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-3 col-sm-6 col-xs-12">
                <?php get_sidebar("search"); ?>
            </div>
        </div>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" title="<?php bloginfo('description'); ?>" href="<?php echo esc_url(home_url('/')); ?>">
                        <img id="logo" height="70" width="70"
                             src="<?php echo get_bloginfo('template_directory'); ?>/img/logo.svg"/>
                    </a>
                </div>
                <?php
                wp_nav_menu( array(
                        'theme_location'    => 'primary',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'bs-example-navbar-collapse-1',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker())
                );
                ?>
            </div>
        </nav>
    </header>

	<?php

	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>

	<div>
		<div id="content">
