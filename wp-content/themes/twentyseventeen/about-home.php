<?php

//Define your custom post type name in the arguments

$args = array('post_type' => 'aboutHome', 'order' => 'ASC');

//Define the loop based on arguments

$loop = new WP_Query($args);
$count_posts = wp_count_posts('aboutHome')->publish;
?>
<?php while ($loop->have_posts()) : $loop->the_post(); ?>
    <section class="wow fadeIn" id="arts" data-wow-offset="100">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="heading">
                        <h2 class="text-center pink-color"><?= the_title(); ?></h2>
                        <div class="fancy-heading"></div>
                    </div>

                </div>
                <div class="row">
                    <?= the_content(); ?>
                </div>
            </div>
        </div>
    </section>
<?php endwhile;
