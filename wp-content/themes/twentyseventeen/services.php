<?php

$args = array('post_type' => 'services', 'order' => 'ASC');

//Define the loop based on arguments

$loop = new WP_Query($args);
$count_posts = wp_count_posts('services')->publish;
?>
    <div class="services-home-wrapper">
        <?php while ($loop->have_posts()) : $loop->the_post(); ?>
            <?php $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>

            <div class="service-wrap">
                <div class="img-box">
                    <a href="<?= get_permalink(); ?>">
                        <?= the_post_thumbnail('medium'); ?>
                    </a>
                </div>
                <div class="service-title">
                    <h3><?= the_title(); ?></h3>
                </div>
                <div class="service-content">
                    <?php
                    $content = get_the_excerpt();
                    echo strlen($content) > 200 ? mb_substr($content, 0, 200) . "..." : $content;
                    ?>
                </div>
                <div class="service-btn">
                    <a href="<?= get_permalink(); ?>" class="service-read-more">
                        <?php _e('Read More', 'simple-bootstrap') ?>
                    </a>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
