
<?php if ( is_active_sidebar( 'sidebar-search' ) ) { ?>
    <div id="sidebar-search" role="complementary">
        <div>
            <?php dynamic_sidebar( 'sidebar-search' ); ?>
        </div>
    </div>
<?php } ?>