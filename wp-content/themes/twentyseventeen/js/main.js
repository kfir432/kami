var Main = {
  selectedMenuItem: null,
  init: function() {
    // Main.homeMenuSettings();
    Main.attachEvents();
    Main.scrollToTop();
    Main.scrollToTopInit();

  },
  attachEvents: function() {
      jQuery("#arrow-down div").on("click", Main.scrollToElem);
  },
  subMenu: function() {
    if (jQuery(this).hasClass("open")) {
      jQuery(this).removeClass("open");
    } else {
      jQuery(this).addClass("open");
    }
  },

  scrollToTopInit: function() {
    jQuery(".scroll-top,.scroll-top-mobile").click(function() {
      jQuery("html, body").animate(
        {
          scrollTop: 0
        },
        "slow"
      );
      return false;
    });
  },
  scrollToTop: function() {
    jQuery(window).scroll(function() {
      jQuery(".scroll-top").hide();
      if (jQuery(this).scrollTop() > 100) {
        jQuery(".scroll-top").fadeIn();
        jQuery("nav.navbar").addClass("nav-drag");
      } else {
        jQuery(".scroll-top").fadeOut();
        jQuery("nav.navbar").removeClass("nav-drag");
      }
    });
  },
  scrollToElem: function(e) {
    e.preventDefault();
    var elem = jQuery(this).attr("title");
    jQuery("html,body").animate(
      {
        scrollTop: jQuery("#" + elem).offset().top + -100
      },
      "slow"
    );
  }
};

jQuery(document).ready(function() {
  Main.init();
});
