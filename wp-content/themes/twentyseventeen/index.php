<?php get_header(); ?>

    <div id="content">
        <section id="main-banner" role="main">
            <?php echo do_shortcode('[rev_slider alias="news-gallery3"]'); ?>
        </section>
        <div class="clearfix"></div>
        <section id="about-home">
            <?php include 'about-home.php'; ?>
        </section>
        <div class="clearfix"></div>
        <section id="services" class="grey-bg">
            <?php include 'services.php'; ?>
        </section>
        <div class="clearfix"></div>
        <section id="testimonials" class="container-fluid">
            <?php include 'testimonials.php'; ?>
        </section>
    </div>

<?php get_footer(); ?>