<?php get_header(); ?>

    <div id="content">
        <section id="main-banner" role="main">
<!--                --><?php //echo do_shortcode('[rev_slider alias="news-gallery3"]'); ?>
                <?php echo do_shortcode('[cycloneslider id="main"]'); ?>
        </section>
        <div class="clearfix"></div>
        <section id="about-home">
            <?php include 'about-home.php'; ?>
        </section>
        <div class="clearfix"></div>
        <section id="services" class="grey-bg">
            <div class="row">
                <div class="heading">
                    <h3 class="center block-head pink-color"><span class="main-color">
                        <?php _e('What Can I Help You ?', 'simple-bootstrap'); ?></h3>
                </div>

            </div>
            <?php include 'services.php'; ?>
        </section>
        <div class="clearfix"></div>
        <section id="testimonials" class="container-fluid">
            <?php include 'testimonials.php'; ?>
        </section>
        <div class="clearfix"></div>
<!--        <section id="specialities" class="container-fluid">-->
<!--            --><?php //include 'specialties.php'; ?>
<!--        </section>-->
        <section class="parallax-box module parallax parallax-box2" id="contact-us">
            <div class="parallax-content">
                <div class="container wow fadeIn" data-wow-offset="100">
                    <?php include 'contactform.php'; ?>
                </div>
            </div>
        </section>
    </div>

<?php get_footer(); ?>