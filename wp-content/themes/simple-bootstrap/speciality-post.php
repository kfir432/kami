<?php
/**
 * WP Post Template: Speciality Template
 */

get_header(); ?>

    <section class="service-post-section">
        <?php
        if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="wow fadeIn" data-wow-offset="100">
                <div class="container-fluid service-wrapper">
                    <div class="container">
                        <div class="breadcrumbs col-xs-12">

                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                            <div>
                                <h1 class="inline-block blog-title">
                                    <a href="<?= get_permalink(); ?>" class="orange-text">
                                        <?= the_title(); ?>
                                    </a>
                                </h1>
                                <div class="blog-date">
                                    <?= get_the_date(); ?>
                                </div>
                            </div>
                            <hr/>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="the-post-content no-border no-padding">
                                <div class="blog-thumb">
                                    <?= the_post_thumbnail('medium'); ?>
                                </div>
                                <?= the_content(); ?>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>


        <?php endwhile; ?>
        <?php endif; ?>
    </section>
<?php get_footer(); ?>