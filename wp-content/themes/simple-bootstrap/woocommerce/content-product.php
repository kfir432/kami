<?php $new_posts = new WP_Query(
    array(
        'post_type' => 'product', //post of page of my post type
        'cat' => 0, // category id, 0 for all categories.
        'posts_per_page' => 50,
        'offset' => 0, //how many posts you want to eliminate from the query
        'orderby' => '', // order by title or date ?
        'order' => 'DESC'
    ) // order as ASC or DESC
);
global $product;
$attachment_ids = $product->get_gallery_attachment_ids();
?>

<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="product-item">
        <a id="id-<?php the_id(); ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

            <div class="product title">
                <?php the_title(); ?>
            </div>
            <div class="rotator image" data-cycle-timeout="1200" data-cycle-swipe=true
                 data-cycle-swipe-fx=scrollHorz data-cycle-pause-on-hover="false">
                <div class="cycle-pager"></div>
                <?php foreach ($attachment_ids as $attachment_id): ?>
                    <img data-src="<?= $image_link = wp_get_attachment_image_src($attachment_id, 'large')[0]; ?>"/>
                <?php endforeach; ?>
            </div>
            <div class="price">
                <span><?php echo $product->get_price_html(); ?></span>
            </div>
        </a>

    </div><!-- /col-md-3 -->
</div>


