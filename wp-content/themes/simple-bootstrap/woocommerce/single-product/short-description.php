<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $post, $product;

if (!$post->post_excerpt) {
    return;
}
$count = get_post_meta($post->ID, 'total_sales', true);
$string = wc_get_rating_html($rating, $count);
$average = $product->get_average_rating();

?>
<div class="product-content">
    <hr/>
    <div class="content-title">
        <h2 class="inline-block"><?php _e("Product Description", "simple-bootstrap"); ?></h2>
<!--        <div class="star-rating">-->
<!--            --><?php //echo '<span style="width:' . (($average / 5) * 100) . '%">
//<strong itemprop="ratingValue" class="rating">' . $average . '</strong> ' . __('out of 5', 'woocommerce') . '</span>';
//            ?>
<!--        </div>-->
    </div>
    <div class="clearfix"></div>
    <div class="the-content">
        <?php
        the_content();
        ?>
    </div>
    <!--    <div class="total-sales">-->
    <!--        <h4>--><?//= $count ?><!-- --><?php //_e("Total Sales", "simple-bootstrap"); ?><!--</h4>-->
    <!--    </div>-->
    <div class="size-info">
    </div>
</div>

