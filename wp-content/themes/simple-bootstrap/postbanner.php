<div class="container-fluid margin-bottom-25">
    <div class="post-banner">
        <?php if (get_field('postbanner') == true): ?>
            <div class="parallax-box module parallax" style="
                background:url(<?= $postbanner->url ?>)no-repeat center center scroll;
                background-position: 50% 50%;
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-size: cover;
                ">
                <div class="parallax-content">
                    <div class="container wow fadeIn" data-wow-offset="100">
                        <h1><?= get_field('bannertext') == true ? get_field('bannertext') : the_title(); ?></h1>
                    </div>
                </div>
                <div class="filter-bg"></div>
            </div>
        <?php else : ?>

        <?php endif; ?>
    </div>
</div>