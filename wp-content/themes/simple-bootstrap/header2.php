<!doctype html>  

<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php wp_head(); ?>
</head>
	
<body <?php body_class(); ?>>

	<div id="content-wrapper">

		<header>
            <div id="top-strip">
                <div class="col-md-6 col-sm-6 col-xs-12 contact-line-wrapper">
                    <div class="contact-line">
                        <a href="mailto:<?php _e('Email Address', 'simple-bootstrap'); ?>">
                            <i class="fa fa-envelope"></i>
                            <span><?php _e('Email Address', 'simple-bootstrap'); ?></span>
                        </a>
                    </div>
                    <div class="contact-line">
                        <a href="tel:<?php _e('Phone Number', 'simple-bootstrap'); ?>">
                            <i class="fa fa-phone"></i>
                            <span>
                                <?php _e('Contact Us', 'simple-bootstrap'); ?>
                                <?php _e('Phone Number', 'simple-bootstrap'); ?>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-3 col-sm-6 col-xs-12">
                    <?php get_sidebar("search"); ?>
                </div>
            </div>
			<nav class="navbar navbar-default navbar-static-top">
				<div class="container-fluid">
		  
					<div class="navbar-header">
						<?php if (has_nav_menu("main_nav")): ?>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-responsive-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<?php endif ?>
						<a class="navbar-brand" title="<?php bloginfo('description'); ?>" href="<?php echo esc_url(home_url('/')); ?>">
                            <img id="logo" height="70" width="70"
                                 src="<?php echo get_bloginfo('template_directory'); ?>/img/logo.svg"/>
                        </a>
					</div>
                    <div class="socials-wrap">
                        <?php include 'socialsBar.php';?>
                    </div>
					<?php if (has_nav_menu("main_nav")): ?>
					<div id="navbar-responsive-collapse" class="collapse navbar-collapse">
						<?php
						    simple_bootstrap_display_main_menu();
						?>

					</div>
					<?php endif ?>

				</div>
			</nav>
		</header>

        <?php if (has_header_image()): ?>
        <div class="header-image-container">
            <div class="header-image" style="background-image: url(<?php echo get_header_image(); ?>)">
                <div class="container">
                    <?php if (display_header_text()): ?>
                    <div class="site-title" style="color: #<?php header_textcolor(); ?>;"><?php bloginfo('name') ?></div>
                    <div class="site-description" style="color: #<?php header_textcolor(); ?>;"><?php bloginfo('description') ?></div>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <?php endif ?>
		
		<div id="page-content">
			<div id="page-spacer">
            </div>
