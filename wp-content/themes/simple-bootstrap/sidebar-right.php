
<?php if ( is_active_sidebar( 'sidebar-right' ) ) { ?>
    <div id="sidebar-right" class="" role="complementary">
        <div class="">
            <?php dynamic_sidebar( 'sidebar-right' ); ?>
        </div>
    </div>
<?php } ?>