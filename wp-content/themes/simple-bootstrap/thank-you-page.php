<?php
/*
Template Name: Thank You Page
*/
?>
<?php include 'header2.php' ?>
    <section class="post-section">
        <?php if (have_posts()) : while (have_posts()) : the_post();
            $postbanner = (object)get_field('postbanner');
            ?>

            <?php include 'postbanner.php' ?>
            <div class="clearfix"></div>
            <div class="wow fadeIn" data-wow-offset="100">
                <div class="container the-content">
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                        <div class="the-post-content no-border no-padding">
                            <?= the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>


        <?php endwhile; ?>
        <?php endif; ?>
    </section>
    <div class="clearfix"></div>
<?php get_footer(); ?>