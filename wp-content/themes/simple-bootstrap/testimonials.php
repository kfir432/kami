<!-- Carousel -->
<div class="carousel slide col-md-8 col-sm-7 col-xs-12" data-ride="carousel" id="quote-carousel">
    <div class="carousel-control-wrapper">
        <h3 class="block-head pink-color"><?php _e('Testimonials', 'simple-bootstrap') ?></h3>
        <div class="carousel-control-wrap">
            <a class="right carousel-control" href="#quote-carousel" role="button" data-slide="next">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
            <a class="left carousel-control" href="#quote-carousel" role="button" data-slide="prev">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <!-- Controls -->


        <?php
        // Item size (set here the number of posts for each group)
        $i = 4;

        // Set the arguments for the query
        global $post;


        $args = array(
            'numberposts' => -1, // -1 is for all
            'post_type' => 'clients', // or 'post', 'page'
            'orderby' => 'title', // or 'date', 'rand'
            'order' => 'ASC', // or 'DESC'
        );

        // Get the posts
        $myposts = get_posts($args);

        function calcStars($number)
        {
            $html = "";
            for ($i = 0; $i < 5; $i++) {
                $class = $number > $i ? "colored" : "";
                $html .= '<i class="fa fa-star ' . $class . ' " aria-hidden="true"></i>';
            }
            return $html;
        }

        // If there are posts
        if ($myposts):

            // Groups the posts in groups of $i
            $chunks = array_chunk($myposts, $i);
            $html = "";

            /*
             * Item
             * For each group (chunk) it generates an item
             */
            foreach ($chunks as $chunk):
                // Sets as 'active' the first item
                ($chunk === reset($chunks)) ? $active = "active" : $active = "";
                $html .= '<div class="item ' . $active . '"><div><div class="row">';

                /*
                 * Posts inside the current Item
                 * For each item it generates the posts HTML
                 */
                foreach ($chunk as $post):
                    $content = get_the_excerpt();
                    $out = strlen($content) > 250 ? mb_substr($content, 0, 250) . "..." : $content;

                    $html .= '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">';
                    $html .= '<div class="carousel-item">';
                    $html .= get_the_post_thumbnail($post->ID);
                    $html .= '<p>' . $out . '</p>';
                    $html .= calcStars(get_field('stars'));
                    $html .= '</div>';
                    $html .= '<div class="testimonials-name">';
                    $html .= get_the_title($post->ID);
                    $html .= '</div>';
                    $html .= '</div>';
                endforeach;

                $html .= '</div></div></div>';

            endforeach;

            // Prints the HTML
            echo $html;

        endif;
        ?>

    </div> <!-- carousel inner -->

</div> <!-- /carousel -->
<div class="col-md-4 col-sm-5 col-xs-12 iframe-center">
    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FCami2baly%2F&tabs=timeline&width=350&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=143315755762761"
            width="350" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
            allowTransparency="true"></iframe>
</div>
