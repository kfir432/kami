<div class="specialities-text-widget col-md-4 col-sm-3 col-xs-12">
    <h3 class="block-head">
        <span><?php _e('Specialities', 'simple-bootstrap') ?></span>
        <span class="pink-color"><?php _e('Last', 'simple-bootstrap') ?></span>
    </h3>
    <?php get_sidebar("speciality"); ?>

    <div class="carousel-control-wrapper">
        <div class="carousel-control-wrap">
            <a class="right carousel-control" href="#specialities-carousel" role="button" data-slide="next">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
            <a class="left carousel-control" href="#specialities-carousel" role="button" data-slide="prev">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>
<!-- Carousel -->
<div class="carousel slide col-md-8 col-sm-9 col-xs-12" data-ride="carousel" id="specialities-carousel">

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <!-- Controls -->

        <?php
        // Item size (set here the number of posts for each group)
        $i = wpmd_is_phone() === true ? 1 : 3;

        // Set the arguments for the query
        global $post;


        $args = array(
            'numberposts' => -1, // -1 is for all
            'post_type' => 'specialties', // or 'post', 'page'
            'orderby' => 'title', // or 'date', 'rand'
            'order' => 'ASC', // or 'DESC'
        );

        // Get the posts
        $myposts = get_posts($args);

        // If there are posts
        if ($myposts):

            // Groups the posts in groups of $i
            $chunks = array_chunk($myposts, $i);
            $html = "";

            /*
             * Item
             * For each group (chunk) it generates an item
             */
            foreach ($chunks as $chunk):
                // Sets as 'active' the first item
                ($chunk === reset($chunks)) ? $active = "active" : $active = "";
                $html .= '<div class="item ' . $active . '"><div><div class="row">';

                /*
                 * Posts inside the current Item
                 * For each item it generates the posts HTML
                 */
                foreach ($chunk as $post):
                    $html .= '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">';
                    $html .= '<div class="carousel-item">';
                    $html .= '<div class="carousel-image-item">';
                    $html .= '<a href="' . get_permalink() . '">';
                    $html .= get_the_post_thumbnail($post->ID, 'medium');
                    $html .= '</a>';
                    $html .= '</div>';
                    $html .= '<h4><a href="' . get_permalink() . '">' . get_the_title($post->ID) . '</a></h4>';
                    $html .= '</div>';
                    $html .= '</div>';
                endforeach;

                $html .= '</div></div></div>';

            endforeach;

            // Prints the HTML
            echo $html;

        endif;
        ?>

    </div> <!-- carousel inner -->

</div> <!-- /carousel -->

