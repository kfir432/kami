
<?php if ( is_active_sidebar( 'sidebar-speciality' ) ) { ?>
    <div id="sidebar-speciality" role="complementary">
        <div>
            <?php dynamic_sidebar( 'sidebar-speciality' ); ?>
        </div>
    </div>
<?php } ?>