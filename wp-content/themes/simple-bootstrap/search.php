<?php get_header(); ?>

    <div id="content" class="row">
    <div class="page-top-image margin-bottom-25">
        <h1 class="text-center"><?php _e("Search Results", "simple-bootstrap"); ?></h1>
    </div>
    <div id="main" class="container-fluid" role="main">

        <div class="col-md-8 col-sm-8 col-xs-12">
            <?php if (have_posts()) : ?>

                <?php while (have_posts()) : the_post(); ?>
                    <div class="search-items-wrapper">
                        <?php simple_boostrap_display_post(true); ?>
                    </div>

                <?php endwhile; ?>

                <?php simple_boostrap_page_navi(); ?>

            <?php else : ?>

                <!-- this area shows up if there are no results -->

                <article id="post-not-found" class="block">
                    <p><?php _e("No items found.", "simple-bootstrap"); ?></p>
                </article>

            <?php endif; ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php get_sidebar("right"); ?>
        </div>

    </div>

<?php get_footer(); ?>