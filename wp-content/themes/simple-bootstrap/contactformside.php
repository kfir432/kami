<section class="contact-section wow fadeIn" data-wow-offset="100">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12 headers-wrap">
                <h3 class="text-center text-white"><?php _e("CONTACT US", "simple-bootstrap"); ?></h3>
            </div>
            <div class="row">
                <div class="socials-wrap">
                    <?php include 'socialsBar.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="contact-info col-md-4">
                    <span><?php _e("Address", "simple-bootstrap"); ?></span>
                </div>
                <div class="contact-info col-md-4">
                    <a href="tel:‫052-224-949" onclick="ga('send', 'event', 'button', 'onClick', 'Call');">‫052-224-949</a>
                </div>
                <div class="contact-info col-md-4">
                    <a href="mailto:‫‫cami2baly@hotmail.com"><?php _e("Email Address", "simple-bootstrap"); ?></a>
                </div>
            </div>
            <div class="row">
                <?php
                echo do_shortcode('[contact-form-7 id="41" title="heSide"]');
                ?>
            </div>
        </div>
    </div>
</section>

