		
        </div>


        <footer>
            <div id="inner-footer" class="vertical-nav">
                <div class="container">
                    <div class="row">
                        <?php dynamic_sidebar('footer1'); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 navbar-inverse navbar-fixed-bottom mobile-footer">
                <div class="row" id="bottomNav">
                    <div class="col-xs-4 text-center"><a href="#scroll-top" class="scroll-top-mobile"><i
                                    class="fa fa-chevron-up"></i></a></div>
                    <div class="col-xs-4 text-center"><a href="tel:<?php _e("Phone Number", "simple-bootstrap"); ?>" onclick="ga('send', 'event', 'button', 'onClick', 'Call');"><i class="fa fa-phone"></i></a></div>
                    <div class="col-xs-4 text-center"><a href="mailto:cami2baly@hotmail.com"><i class="fa fa-envelope"></i></a>
                    </div>
                    <div class="col-xs-12 text-center mobile-copyright">
                        <p><?php _e("copyrights", "simple-bootstrap"); ?>&nbsp;<?php echo date("Y"); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center copyright">
                <p><?php _e("copyrights", "simple-bootstrap"); ?>&nbsp;<?php echo date("Y"); ?>
                <br/>
                    <a target="_blank" href="http://www.we-do.media/"><?php _e("wedomedia", "simple-bootstrap"); ?></a>
                </p>

            </div>
        </footer>

        </div>
        <div id="tooper">
            <div class="grid col-300 scroll-top"><a href="#scroll-top"
                                                    title="<?php esc_attr_e('scroll to top', 'simple-bootstrap'); ?>"><i
                            class="fa fa-arrow-up"></i></a></div>
        </div>

    </div>

	<?php wp_footer(); // js scripts are inserted using this function ?>
           <script>
               new WOW().init();
           </script>
</body>

</html>
