<?php
/*
Template Name: Contact Page
*/

?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post();
    $postbanner = (object)get_field('postbanner');
    ?>
    <section id="contact-page">
        <div class="container-fluid">
            <?php woocommerce_breadcrumb(); ?>
        </div>
        <div class="clearfix"></div>
        <?php include 'postbanner.php' ?>
        <div class="clearfix"></div>
        <div class="wow fadeIn" data-wow-offset="100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <?php include 'contactform.php'; ?>
                    </div>
                    <div class="col-md-4 woocommmerce-side-bar col-sm-12 col-xs-12">
                        <?php get_sidebar("right"); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; ?>

<?php else : ?>

    <article id="post-not-found" class="block">
        <p><?php _e("No pages found.", "simple-bootstrap"); ?></p>
    </article>

<?php endif; ?>

<div class="clearfix"></div>

<?php get_footer(); ?>
