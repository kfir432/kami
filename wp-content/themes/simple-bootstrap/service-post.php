<?php
/**
 * WP Post Template: Service Template
 */

get_header(); ?>

    <section class="service-post-section">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();
            $postbanner = (object)get_field('postbanner');
            ?>
            <div class="breadcrumbs col-xs-12">
                <?php woocommerce_breadcrumb(); ?>
            </div>
            <div class="clearfix"></div>
            <?php include 'postbanner.php' ?>
            <div class="clearfix"></div>
            <!--            <div class="page-top-image">-->
            <!--                <h1>--><? //= the_title(); ?><!--</h1>-->
            <!--            </div>-->

            <div class="clearfix"></div>

            <div class="wow fadeIn" data-wow-offset="100">
                <div class="container-fluid service-wrapper">
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="the-post-content no-border no-padding">
                            <?= the_content(); ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 woocommmerce-side-bar">
                        <?php get_sidebar("right"); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-xs-12">
                <hr/>
                <?php include 'contactformside.php' ?>
            </div>

        <?php endwhile; ?>
        <?php endif; ?>
    </section>
<?php get_footer(); ?>