<?php get_header(); ?>

    <div class="container">
        <section class="post_content">

            <h2 class="text-center">
                <?php _e("Page not found", "simple-bootstrap"); ?>
            </h2>
            <h3 class="text-center">
                <a href="<?php echo esc_url(home_url('/')); ?>">
                    <?php _e("Back to home", "simple-bootstrap"); ?>
                </a>
            </h3>

        </section>
    </div>

<?php get_footer(); ?>