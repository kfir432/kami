<span class="social-icons">
            <?php if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
                $count = WC()->cart->cart_contents_count; ?>
                <a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>"
                   title="<?php _e('View your shopping cart', 'simple-bootstrap'); ?>">
            <?php if ($count > 0) {
                ?>
                <span class="cart-contents-count cart-badge hvr-grow"><?php echo esc_html($count); ?></span>
                <?php
            } ?>
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            </a>
                <?php
            } ?>
                        <a target="_blank"
                           href="https://www.facebook.com/Cami2baly/">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
<!--                        <a target="_blank" href="#">-->
<!--                            <i class="fa fa-linkedin" aria-hidden="true"></i>-->
<!--                        </a>-->
<!--                        <a target="_blank" href="#">-->
<!--                            <i class="fa fa-youtube" aria-hidden="true"></i>-->
<!--                        </a>-->
    <a href="tel:072-3940815" onclick="ga('send', 'event', 'button', 'onClick', 'Call');">
                            <i class="fa fa-mobile" aria-hidden="true"></i>
                        </a>
    
                    </span>